<?php
if (isset($_SERVER["HTTP_X_FORWARDED_PROTO"]) && $_SERVER["HTTP_X_FORWARDED_PROTO"] == "https") $_SERVER["HTTPS"] = "on";

define( 'DB_NAME', 'c9' );
define( 'DB_USER', getenv( 'C9_USER' ) );
define( 'DB_PASSWORD', '' );
define( 'DB_HOST', getenv( 'IP' ) );
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );

define( 'WP_HOME',    'http://' . getenv( 'C9_PROJECT' ) . '.' . getenv( 'C9_USER' ) . '.c9.io' );
define( 'WP_SITEURL', 'http://' . getenv( 'C9_PROJECT' ) . '.' . getenv( 'C9_USER' ) . '.c9.io' );       

define( 'WPLANG', 'cs_CZ' );
define( 'WP_DEBUG', false );
define( 'WP_CACHE', false );
define( 'DISALLOW_FILE_EDIT', true );

$_SERVER["HTTP_HOST"] = $_SERVER["SERVER_NAME"];
$_SERVER["HTTP_HOST"] = $_SERVER["SERVER_NAME"];